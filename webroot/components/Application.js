import { html, render, Component } from '../js/preact-htm.js'

import Title from './Title.js'
import Form from './Form.js'
import Answer from './Answer.js'


class Application extends Component {

    setRefMain = (dom) => this.main = dom
    setRefTitle = (dom) => this.title = dom
    setRefForm = (dom) => this.form = dom
    setRefAnswer = (dom) => this.answer = dom

    constructor(props) {
        super()
    }

    componentDidMount() {
        this.form.register(this.answer)
    }

    render() {
        return html`
        <div ref=${this.setRefMain}>
            <${Title} ref=${this.setRefTitle} />
            <${Form} ref=${this.setRefForm} />
            <hr></hr>
            <${Answer} ref=${this.setRefAnswer}/>
        </div>
        `
    }
}

export default Application


