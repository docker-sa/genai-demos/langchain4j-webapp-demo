import { html, render, Component } from '../js/preact-htm.js'

class Title extends Component {

    constructor(props) {
        super()

        this.state = {
            model: "...",
            baseUrl: "..."
            //temperature: undefined,
            //repeatPenalty: undefined
        }
    }

    async componentDidMount() {
        try {
            const response = await fetch("/model", {
                method: "GET",
                headers: {
                  "Content-Type": "application/json",
                }
              })
            
            const data = await response.json()
            
            console.log("📦 LLM", data)
            this.setState({
                model: data.model,
                baseUrl: data.url
                //temperature: data.temperature,
                //repeatPenalty: data.repeatPenalty
            })
      
          } catch (error) {
            console.log("😡", error) 
          }
    }

    render() {
        return html`
        <div class="container">

            <div class="hero-body">
                <p class="title is-2">
                    ☕️ Java 🐳 GenAI Stack 🦙🦜🔗
                </p>
                <p class="subtitle is-3">
                    With Vert-x, LangChain4J & Docker Compose
                </p>
                <span class="tag is-dark">${this.state.model}</span>|
                <span class="tag is-dark">${this.state.baseUrl}</span>
                <!--
                <span class="tag is-dark">${this.state.temperature}</span>|
                <span class="tag is-dark">${this.state.repeatPenalty}</span>
                -->

            </div>
        </div>
        `
    }
}

export default Title