

class Tools {

    static hello() {
        console.log("HELLO")
    }

    static async completion(system, context, question, answer, aborter, that) {
        let waitingTimer = setInterval(waitingMessage, 500)
        let waiting = true

        function waitingMessage() {
            const d = new Date()
            answer.changeMessageHeader("🤔 computing " + d.toLocaleTimeString())
        }
        
        let responseText = ""


        try {
            const response = await fetch("/prompt", {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                question: question,
                system: system,
                context: context
              }),
              signal: aborter.signal
            })
    
            const reader = response.body.getReader()
    
            while (true) {
              const { done, value } = await reader.read()
    
              if (waiting) {
                clearInterval(waitingTimer)
                waiting = false
                answer.changeMessageHeader("")
              }
              
              if (done) {
                // Do something with last chunk of data then exit reader
                responseText = responseText + "\n"
                answer.changeResponse(markdownit().render(responseText))    
                return
              }
              // Otherwise do something here to process current chunk
              const decodedValue = new TextDecoder().decode(value)
              console.log(decodedValue)
              responseText = responseText + decodedValue
              answer.changeResponse(markdownit().render(responseText))
            }
    
          } catch(error) {
            if (error.name === 'AbortError') {
              console.log("✋", "Fetch request aborted")
              
              that.setState({
                aborter: new AbortController()
              })
        
              try {
                const response = await fetch("/cancel-request", {
                  method: "DELETE",
                })
                console.log(response)
              } catch(error) {
                console.log("😡", error)
              }
    
            } else {
              console.log("😡", error)
            }
          }

    }

}

export default Tools
