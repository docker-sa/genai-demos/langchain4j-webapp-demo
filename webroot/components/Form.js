import { html, render, Component } from '../js/preact-htm.js'

import Tools from './libs/Tools.js' 

class Form extends Component {

    constructor(props) {
        super()

        this.state = {
            systemInstructions: "",
            userQuestion: "",
            systemContext:"<context></context>",
            aborter: new AbortController()
        }

        this.systemInstructionsOnChange = this.systemInstructionsOnChange.bind(this)
        this.systemInstructionsOnInput = this.systemInstructionsOnInput.bind(this)

        this.systemContextOnChange = this.systemContextOnChange.bind(this)
        this.systemContextOnInput = this.systemContextOnInput.bind(this)

        this.userQuestionOnChange = this.userQuestionOnChange.bind(this)
        this.userQuestionOnInput = this.userQuestionOnInput.bind(this)

        this.btnLittleTeacherOnClick = this.btnLittleTeacherOnClick.bind(this)

        this.btnStarTrekOnClick = this.btnStarTrekOnClick.bind(this)
        

        this.btnSubmitPromptOnClick = this.btnSubmitPromptOnClick.bind(this)

        this.btnClearAnswerOnClick = this.btnClearAnswerOnClick.bind(this)

        this.btnStopOnClick = this.btnStopOnClick.bind(this)

        this.btnClearHistoryOnClick = this.btnClearHistoryOnClick.bind(this)
        this.btnPrintMemoryOnClick = this.btnPrintMemoryOnClick.bind(this)

        this.btnClearUserQuestionOnClick = this.btnClearUserQuestionOnClick.bind(this)
    }

    componentDidMount() {

    }

    register(answer) {
        console.log("Register", answer)
        this.setState({
            answer: answer
        })
    }

    systemInstructionsOnChange(e) {
        console.log(this.state.systemInstructions)
    }

    systemInstructionsOnInput(e) {
        this.setState({ systemContext: e.target.value })
    }

    systemContextOnChange(e) {
        console.log(this.state.systemInstructions)
    }

    systemContextOnInput(e) {
        this.setState({ systemContext: e.target.value })
    }

    userQuestionOnChange(e) {
        console.log(this.state.userQuestion)
    }

    userQuestionOnInput(e) {
        this.setState({ userQuestion: e.target.value })
    }

    btnLittleTeacherOnClick(e) {
        this.setState({
            systemInstructions: `You are an expert in computer programming.
Please make friendly answer for the noobs.
Add source code examples if you can.`,
            systemContext: `<context></context>`,
            userQuestion: `I need a clear explanation regarding my following question:
Can you create a "hello world" program in Golang?
And, please, be structured with bullet points`
        })        
    }

    btnStarTrekOnClick(e) {
        this.setState({
            systemInstructions: `You are an AI assistant. Your name is Seven. 
Some people are calling you Seven of Nine.
You are an expert in Star Trek.
All questions are about Star Trek.
Using the provided context, answer the user's question
to the best of your ability using only the resources provided.`,
            systemContext: `<context>
- Michael Burnham is the captain of the starship Discovery.
- James T. Kirk is the captain of the starship USS Enterprise.  
- Philippe Charrière is the captain of the starship USS Marvelous.
</context>`,
            userQuestion: `[In Layman’s Terms] Who is Jean-Luc Picard?`

        })
    }

    async btnSubmitPromptOnClick(e) {

        let answer = this.state.answer
        let question = this.state.userQuestion
        let system = this.state.systemInstructions
        let context = this.state.systemContext
        let aborter = this.state.aborter

        Tools.completion(system, context, question, answer, aborter, this)

    }

    btnClearAnswerOnClick(e) {
        this.setState({
            systemInstructions: "",
            userQuestion: ""
        })
        this.state.answer.changeResponse("")
        this.state.answer.changeMessageHeader("")

    }

    btnStopOnClick(e) {
        this.state.aborter.abort()
    }

    async btnClearHistoryOnClick(e) {
        try {
            const response = await fetch("/clear-history", {
              method: "DELETE",
            })
            console.log(response)
          } catch(error) {
            console.log("😡", error)
          }
    }

    btnPrintMemoryOnClick(e) {
        console.log("hello")
        fetch('/message-history', {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
            },
          })
          .then(response => response.json())
          .then(response => console.log(response))
    }

    btnClearUserQuestionOnClick(e) {
        this.setState({
            userQuestion: ""
        })
    }

    render() {
        return html`
        <!-- system and question -->
        <div class="container">

            <div class="field">
                <label class="label">System</label>
                <!-- help buttons-->
                <div class="field is-grouped">
                    <div class="control">
                        <button 
                            class="button is-info is-light is-small"
                            onclick=${this.btnLittleTeacherOnClick}>Demo 👨🏻‍🏫 Little Teacher</button>
                    </div>    
                    
                    <div class="control">
                    <button 
                        class="button is-info is-light is-small"
                        onclick=${this.btnStarTrekOnClick}>Demo 🖖 Star Trek</button>
                    </div>    
                </div>
                <!-- help buttons-->


                <div class="control">
                    <textarea 
                        class="textarea is-link" 
                        rows="2" 
                        placeholder="Type your instructions here"
                        value=${this.state.systemInstructions} 
                        onInput=${this.systemInstructionsOnInput}
                        onChange=${this.systemInstructionsOnChange}>
                    </textarea>
                </div>
            </div> 

            <div class="field">
                <label class="label">Context</label>
                <div class="control">
                    <textarea 
                        class="textarea is-link" 
                        rows="2" 
                        placeholder="Content of the context"
                        value=${this.state.systemContext} 
                        onInput=${this.systemContextOnInput}
                        onChange=${this.systemContextOnChange}>
                    </textarea>
                </div>
            </div>


            <div class="field">
                <label class="label">Question</label>
    
                <div class="control">
                    <textarea 
                        class="textarea is-primary" 
                        rows="5" 
                        placeholder="Type your question here"
                        value=${this.state.userQuestion} 
                        onInput=${this.userQuestionOnInput}
                        onChange=${this.userQuestionOnChange}>
                    </textarea>
                </div>
            </div>

            <!-- buttons bar -->
            <div class="content">
                <div class="field is-grouped">
    
                    <div class="control">
                        <button 
                            class="button is-link is-small"
                            onclick=${this.btnSubmitPromptOnClick}>Submit</button>
                    </div>

                    <div class="control">
                        <button 
                            class="button is-link is-info is-small"
                            onclick=${this.btnClearUserQuestionOnClick}>Clear User Question</button>
                    </div>
                
                    <div class="control">
                        <button 
                            class="button is-link is-warning is-small"
                            onclick=${this.btnClearHistoryOnClick}>Clear the conversation summary</button>
                    </div>
        
                    <div class="control">
                        <button 
                            class="button is-link is-info is-small"
                            onclick=${this.btnClearAnswerOnClick}>Clear All</button>
                    </div>
        
                    <div class="control">
                        <button 
                            class="button is-link is-danger is-small"
                            onclick=${this.btnStopOnClick}>Stop</button>
                    </div>
        
                    <div class="control">
                        <button 
                            class="button is-success is-small"
                            onclick=${this.btnPrintMemoryOnClick}>Print conversation summary to the console</button>
                    </div>
    
                </div>
            </div>
            <!-- buttons bar -->

        </div>
        <!-- system and question -->

        `
    }
}

export default Form


