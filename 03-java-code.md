# J-Code Teacher
## Chatbot demo with LangChain4J, Ollama and Vert-x
>Java Docker GenAI Stack ☕️🐳🤖🦜🔗🦙


## Java code (the important part)

```java
router.post("/prompt").handler(ctx -> {

    var question = ctx.body().asJsonObject().getString("question");
    var systemContent = ctx.body().asJsonObject().getString("system");
    var contextContent = ctx.body().asJsonObject().getString("context");

    // ----------------------------------------
    // 📝 Build the prompt
    // ----------------------------------------
    SystemMessage systemInstructions = systemMessage(systemContent);
    SystemMessage contextMessage = systemMessage(contextContent);
    UserMessage humanMessage = UserMessage.userMessage(question);

    List<ChatMessage> messages = new ArrayList<>();
    
    messages.add(systemInstructions);
    messages.addAll(memory.messages());
    messages.add(contextMessage);
    messages.add(humanMessage);

    // ----------------------------------------
    // 🧠 Update the memory of the conversation
    // ----------------------------------------
    memory.add(humanMessage);

    HttpServerResponse response = ctx.response();

    // ----------------------------------------
    // 🌍 the response will be streamed
    // ---------------------------------------    
    response
        .putHeader("Content-Type", "application/octet-stream")
        .setChunked(true);

    // ----------------------------------------
    // 🤖 Generate the completion (stream)
    // ----------------------------------------
    streamingModel.generate(messages, new StreamingResponseHandler<AiMessage>() {
        @Override
        public void onNext(String token) {
            if (cancelRequest) {
                cancelRequest = false;
                throw new RuntimeException("🤬 Stop up!");
            }
            response.write(token);
        }

        @Override
        public void onComplete(Response<AiMessage> modelResponse) {
            memory.add(modelResponse.content());
            System.out.println("Streaming completed: " + modelResponse);
            response.end();
        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
        }
    });

});


```