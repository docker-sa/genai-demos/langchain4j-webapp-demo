# J-Code Teacher
## Chatbot demo with LangChain4J, Ollama and Vert-x
>Java Docker GenAI Stack ☕️🐳🤖🦜🔗🦙


## Compose file structure

<img src="./imgs/compose.drawio.png" alt="drawing" width="40%"/>


## LLM: Deepseek-coder:1.3b

<img src="./imgs/deepseek-coder.png" alt="drawing" width="40%"/>


> https://ollama.com/library/deepseek-coder:1.3b


**DeepSeek Coder** is trained from scratch on both `87%` code and `13%` natural language in English and Chinese. Size: **776MB** 🎉


> The "1.3b" in the name of the LLM "deepseek-coder:1.3b" likely refers to the model's parameter size, indicating that it has 1.3 billion parameters. The model's parameter size refers to the number of trainable parameters in the neural network of the language model. These parameters include weights and biases that are adjusted during the training process. The more parameters a model has, the more capacity it has to learn complex patterns and representations in data, which often leads to better performance on various tasks.
>
>- Smaller models (e.g., 125 million parameters) are faster and require less computational resources but may have limited performance.
>- Medium-sized models (e.g., 1-2 billion parameters) strike a balance between performance and resource requirements.
>- Larger models (e.g., 6 billion, 13 billion, or even more) tend to perform better on complex tasks but require significantly more computational power and memory.
>The parameter size is a crucial aspect of a model's architecture, directly influencing its capabilities and the resources needed to deploy and use it effectively.

## `compose.yaml`

```yaml
services:

  ollama:
    profiles: [container]
    image: ollama/ollama:0.3.4
    volumes:
      - ollama-data:/root/.ollama
    ports:
      - 11434:11434

  download-llm:
    profiles: [container]
    image: curlimages/curl:8.6.0
    entrypoint: ["curl", "ollama:11434/api/pull", "-d", "{\"name\": \"${LLM}\"}"]
    depends_on:
      ollama:
        condition: service_started

  web-app:
    profiles: [container, webapp]
    build:
      context: .
      dockerfile: Dockerfile
    environment:
      - OLLAMA_BASE_URL=${OLLAMA_BASE_URL}
      - LLM=${LLM}
      - HTTP_PORT=${HTTP_PORT}
      # host.docker.internal: listening the host from the container
    ports:
      - ${HTTP_PORT}:${HTTP_PORT}
    develop:
      watch:
        - action: rebuild
          path: ./webroot
        - action: rebuild
          path: ./src
          
volumes:
  ollama-data:

```

### GPU Acceleration support (ex: NVIDIA)
> https://dev.to/ajeetraina/running-ollama-with-docker-compose-and-gpus-lkn

```yaml
services:
  ollama:
    container_name: ollama
    image: ollama/ollama:0.3.4
    deploy:
      resources:
        reservations:
          devices:
          - driver: nvidia
            capabilities: ["gpu"]
            count: all  # Adjust count for the number of GPUs you want to use
    volumes:
      - ollama:/root/.ollama
```

### Profiles

#### "container" profile

<img src="./imgs/container.profile.drawio.png" alt="drawing" width="40%"/>

#### "webapp" profile (because of Mac M1, M2, ...)

<img src="./imgs/webapp.profile.drawio.png" alt="drawing" width="40%"/>

