#!/bin/bash
: <<'COMMENT'
This demo shows how to use the context to make the answer more accurate
COMMENT

HTTP_PORT=9999 LLM=tinyllama OLLAMA_BASE_URL=http://host.docker.internal:11434 docker compose --profile webapp up
#HTTP_PORT=9999 LLM=tinyllama OLLAMA_BASE_URL=http://ollama:11434 docker compose --profile container up
