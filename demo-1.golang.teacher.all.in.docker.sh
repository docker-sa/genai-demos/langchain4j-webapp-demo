#!/bin/bash
: <<'COMMENT'
This demo allows to create a conversation with the LLM about source code
COMMENT

HTTP_PORT=8888 LLM=deepseek-coder:1.3b OLLAMA_BASE_URL=http://ollama:11434 docker compose --profile container up